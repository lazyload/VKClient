//
//  Realm.swift
//  GBVK
//
//  Created by Евгений Елчев on 06.10.2017.
//  Copyright © 2017 JonFir. All rights reserved.
//

import Foundation
import RealmSwift

extension Realm {
    
    static func replaceAllObjectOfType<T: Object>(toNewObjects objects: [T]) {
        do {
            let realm = try Realm()
            let oldObjects =  realm.objects(T.self)
            
            realm.beginWrite()
            realm.delete(oldObjects)
            realm.add(objects)
            
            try realm.commitWrite()
        } catch {
            print(error)
        }
    }
    
}
