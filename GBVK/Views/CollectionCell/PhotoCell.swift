//
//  PhotoCell.swift
//  GBVK
//
//  Created by Евгений Елчев on 03.10.2017.
//  Copyright © 2017 JonFir. All rights reserved.
//

import UIKit

class PhotoCell: UICollectionViewCell {
    @IBOutlet weak var photoView: UIImageView!
}
