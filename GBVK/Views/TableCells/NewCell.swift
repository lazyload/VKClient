//
//  NewCell.swift
//  GBVK
//
//  Created by Евгений Елчев on 19.10.2017.
//  Copyright © 2017 JonFir. All rights reserved.
//

import UIKit

class NewCell: UITableViewCell {
    
    @IBOutlet weak var autorAvatar: UIImageView!
    @IBOutlet weak var autorName: UILabel!
    @IBOutlet weak var mainImage: UIImageView!
    @IBOutlet weak var mainText: UILabel!
    @IBOutlet weak var likeCount: UILabel!
    @IBOutlet weak var commentCount: UILabel!
    @IBOutlet weak var repostCount: UILabel!
    @IBOutlet weak var viewsCount: UILabel!

}
