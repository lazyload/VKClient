//
//  Group.swift
//  GBVK
//
//  Created by Евгений Елчев on 03.10.2017.
//  Copyright © 2017 JonFir. All rights reserved.
//

import Foundation
import SwiftyJSON
import RealmSwift

class Group: Object {
    @objc dynamic var id = 0
    @objc dynamic var name = ""
    @objc dynamic var photoUrl = ""
    @objc dynamic var count = 0
    
    convenience init(json: JSON) {
        self.init()
        name = json["name"].stringValue
        photoUrl = json["photo_100"].stringValue
        count = json["members_count"].intValue
        id = json["id"].intValue
    }
}
