//
//  photo.swift
//  GBVK
//
//  Created by Евгений Елчев on 03.10.2017.
//  Copyright © 2017 JonFir. All rights reserved.
//

import Foundation
import SwiftyJSON
import RealmSwift

class Photo: Object {
    @objc dynamic var url = ""
    
    convenience init(json: JSON) {
        self.init()
        url = json["photo_130"].stringValue
    }
}
