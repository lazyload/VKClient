//
//  SearchGroups.swift
//  GBVK
//
//  Created by Евгений Елчев on 03.10.2017.
//  Copyright © 2017 JonFir. All rights reserved.
//

import UIKit

class SearchGroups: UITableViewController {
    @IBOutlet weak var searchBar: UISearchBar! {
        didSet {
            searchBar.delegate = self
        }
    }
    
    var environment: Environment {
        return EnvironmentImp.Debug()
    }
    
    let photoService = PhotoService()
    
    lazy var groupService: GroupService? = {
        guard let tabsVC = navigationController?.tabBarController as? TabsVC else { return nil}
        let groupService = GroupService(environment: EnvironmentImp.Debug(), token: tabsVC.token)
        return groupService
    }()
    
    var groups: [Group] = []
    var groupsPhoto: [Int: UIImage] = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return groups.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "GroupCellWithUserCount", for: indexPath) as! GroupCellWithUserCount
        
        let group = groups[indexPath.row]
        
        cell.nameView.text = group.name
        cell.countView.text = String(group.count)
        
        if let photo = groupsPhoto[indexPath.row] {
            cell.avatarView.image = photo
        } else {
            loadPhotos(indexPath: indexPath)
        }

        return cell
    }
    
    func loadPhotos(indexPath: IndexPath) {
        photoService.downloadPhoto(byUrl: groups[indexPath.row].photoUrl) { [weak self] image in
            self?.groupsPhoto[indexPath.row] = image
            self?.tableView.reloadRows(at: [indexPath], with: .none)
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let id = groups[indexPath.row].id
        groupService?.joinToGroup(groupID: id) { [weak self] in
            self?.performSegue(withIdentifier: "addGroup", sender: nil)
        }
    }

}

extension SearchGroups: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard
            let text = searchBar.text,
            !text.isEmpty else {
                
            tableView.reloadData()
            return
        }
        searchGroups(request: text)
        tableView.reloadData()
    }
    
    func searchGroups(request: String) {
        groupService?.searchGroups(request: request) { [weak self] groups in
            self?.groups = groups
            self?.tableView.reloadData()
        }
    }
}
