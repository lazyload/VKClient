//
//  PhotoService.swift
//  GBVK
//
//  Created by Евгений Елчев on 03.10.2017.
//  Copyright © 2017 JonFir. All rights reserved.
//

import Foundation
import Alamofire

struct PhotoService {
    
    func downloadPhoto(byUrl url: String, completion: @escaping (UIImage) -> Void) {
        Alamofire.request(url).responseData { response in
            
            guard
                let data = response.data,
                let image = UIImage(data: data) else { return }
            
            completion(image)
            
        }
    }
    
}
