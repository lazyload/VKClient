//
//  UserService.swift
//  GBVK
//
//  Created by Евгений Елчев on 03.10.2017.
//  Copyright © 2017 JonFir. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import RealmSwift

struct UserService {
    
    private let router: UserRouter
    
    init(environment: Environment, token: String) {
        router = UserRouter(environment: environment, token: token)
    }
    
    func dowloadFriends() {
        Alamofire.request(router.userList()).responseData { response in
            
            guard let data = response.value else { return }
            let json = JSON(data: data)
            let users = json["response"]["items"].array?.flatMap { User(json: $0) } ?? []
            
            Realm.replaceAllObjectOfType(toNewObjects: users)
        }
    }
    
    func downloadPhoto(forUser user: Int, completion: @escaping ([Photo]) -> Void) {
        Alamofire.request(router.userPhotoList(ownerId: user)).responseData { response in
            
            guard let data = response.value else { return }
            let json = JSON(data: data)
            let photos = json["response"]["items"].array?.flatMap { Photo(json: $0) } ?? []
            completion(photos)
        }
    }
    
    func loadFriends() -> [User] {
        do {
            let realm = try Realm()
            return Array(realm.objects(User.self))
        } catch {
            print(error)
            return []
        }
    }
    
    
}
